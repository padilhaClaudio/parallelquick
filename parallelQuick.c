#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <time.h>

// Reads elements from file and returns an array
int * readData(FILE * data, int size)
{
    int * ret = (int*) malloc(size * sizeof(int));

    for (int i = 0; i < size; i++)
        fscanf(data, "%d", &ret[i]);

    return ret;
}

// Split local buffer according to pivot, returns the first element's index on the right side of the division
void splitLocalBuffer (int * local_buffer, int pivot, int * local_buffer_size, MPI_Comm current_comm, MPI_Status status)
{
    int group_size, group_rank;
    MPI_Comm_size(current_comm, &group_size);
    MPI_Comm_rank(current_comm, &group_rank);
    int pair_mask = group_size/2;

    int right_position = *local_buffer_size - 1;

    // Each process splits its local buffer
    for (int i = 0; i <= right_position;)
    {
        if (local_buffer[i] < pivot)
        {
            int aux = local_buffer[i];
            local_buffer[i] = local_buffer[right_position];
            local_buffer[right_position] = aux;
            right_position --;

            if (right_position == i - 1){
                right_position ++;
                break;
            }
        }
        else
        {
            i++;
            if (i == right_position + 1)
            {
                right_position ++;
                break;
            }
        }   
    }
    MPI_Barrier(current_comm);

    if (group_rank < pair_mask) 
    {
        int count;
        int * aux_buffer = malloc(20 * *local_buffer_size * sizeof(int));                                    // Used to get the right position of the array, both to send and receive operations
        memcpy(aux_buffer, local_buffer, *local_buffer_size*(sizeof(int)));   
        aux_buffer += right_position;                   
        MPI_Send(aux_buffer, *local_buffer_size - right_position, MPI_INT, group_rank + pair_mask, group_rank, current_comm);

        MPI_Recv(local_buffer + right_position,20 *  *local_buffer_size, MPI_INT, group_rank + pair_mask, MPI_ANY_TAG, current_comm, &status);
        MPI_Get_count(&status, MPI_INT, &count);
        *local_buffer_size +=  -(*local_buffer_size - right_position) + count;     // Fix local buffer length
    }
    else
    {
        int count;
        int * aux_recv_buffer = (int *) malloc(20* *local_buffer_size * sizeof(int));
        int * aux_send_buffer = (int *) malloc(*local_buffer_size * sizeof(int));
        memcpy(aux_send_buffer, local_buffer, *local_buffer_size*(sizeof(int)));
        MPI_Recv(aux_recv_buffer, 20*(*local_buffer_size), MPI_INT, group_rank - pair_mask, MPI_ANY_TAG, current_comm, &status);
        MPI_Get_count(&status, MPI_INT, &count);
        MPI_Send(aux_send_buffer,*local_buffer_size - (*local_buffer_size - right_position), MPI_INT, group_rank - pair_mask, group_rank, current_comm);
        int i;
        for (i = 0; (right_position + i) < *local_buffer_size ; i++)            // Moves the local buffer items to the begining of the array
            local_buffer[i] = local_buffer[right_position + i];

        *local_buffer_size +=  -(*local_buffer_size - (*local_buffer_size - right_position)) + count;     // Fix local buffer length

        for (int j = 0; j < count; j++)
        {
            local_buffer[i] = aux_recv_buffer[j];                               // Puts the received items in the local buffer
            i++;
        }  
    }
}

// Divide processes into groups and chooses the new pivot
void groupProcesses (MPI_Comm current_comm, int new_group_size, MPI_Comm * new_comm, int my_id)
{
    int group_rank, group_size, my_group;
    MPI_Comm_rank(current_comm, &group_rank);
    MPI_Comm_size(current_comm, &group_size);

    my_group = my_id / new_group_size;
    
    MPI_Barrier(current_comm);
    MPI_Comm_split(current_comm, my_group, group_rank, new_comm);
    MPI_Barrier(*new_comm);
}

int main(int argc, char **argv)
{
    srand(time(NULL));

    FILE * data = fopen(argv[1], "r");
    int size;
    fscanf(data, "%d", &size);
    int * array = readData(data, size);
    int num_procs, my_id;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_id);

    // Processes variables
    MPI_Status status;
    MPI_Comm current_comm = MPI_COMM_WORLD;
    MPI_Comm new_comm;
    int init_division = size / num_procs;
    int * local_buffer = (int*) malloc(size * sizeof(int));
    int pivot;
    int local_buffer_size;
    int new_group_size;

    if (my_id == 0)
    {   
        memcpy(local_buffer, array, (init_division + (size % num_procs) )* sizeof(int));                              // Processor 0 gets his share of the data

        int * ptr_aux = array + (init_division + (size % num_procs) );

        local_buffer_size = init_division + (size % num_procs);
        
        for (int i = 1; i < num_procs; i++)
        {
            MPI_Send(ptr_aux, init_division, MPI_INT, i, 0, MPI_COMM_WORLD);
            ptr_aux += init_division;
        } 

        int r = rand()%size;
        pivot = array[r];
    }
    else
    {
        MPI_Recv(local_buffer, init_division, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);      // Receives the elements  
        local_buffer_size = init_division;
    } 
    MPI_Barrier(MPI_COMM_WORLD);

    if (my_id == 0)
        free(array);

    MPI_Bcast(&pivot, 1, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);                                                                      

    splitLocalBuffer(local_buffer, pivot, &local_buffer_size, MPI_COMM_WORLD, status);

    new_group_size = num_procs/2;
    MPI_Barrier(MPI_COMM_WORLD);

    while (new_group_size > 1)
    {

        groupProcesses(current_comm, new_group_size, &new_comm, my_id);

        MPI_Barrier(MPI_COMM_WORLD);

        int group_rank, group_size, process_of_pivot, pivot_index;  
        MPI_Comm_rank(new_comm, &group_rank);
        MPI_Comm_size(new_comm, &group_size);
       
        if (group_rank == 0)
        {
            process_of_pivot = rand() % group_size;
        }

        MPI_Barrier(new_comm);
        MPI_Bcast(&process_of_pivot, 1, MPI_INT, 0, new_comm);
        MPI_Barrier(new_comm);
        if (group_rank == process_of_pivot  && local_buffer_size > 0) 
        {
            pivot_index = rand() % local_buffer_size;
            pivot = local_buffer[pivot_index];
        }else{

        }
        MPI_Barrier(new_comm);
        MPI_Bcast(&pivot, 1, MPI_INT, process_of_pivot, new_comm);
        MPI_Barrier(new_comm);

        splitLocalBuffer(local_buffer, pivot, &local_buffer_size, new_comm, status); 
        MPI_Barrier(new_comm);
        current_comm = new_comm;
        new_group_size = new_group_size/2;
        MPI_Barrier(MPI_COMM_WORLD);
    }

    // TODO: each process sorts its local array
   for (int i = 0; i < num_procs; i ++)
   {    
       if (my_id == i)
       {
            for (int j = 0; j < local_buffer_size; j++)
                printf("%d ", local_buffer[j]);
            printf("\n");
       }

       MPI_Barrier(MPI_COMM_WORLD);
    }   
    MPI_Finalize();
    return 0;
}